<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Allura&display=swap" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body>
        <!-- Opening Cover-->
        <div id="index-banner" class="parallax-container">
            <div class="section no-pad-bot">
                <div class="container">
                    <br><br>
                    <div class="row center">
                        <h5 class="header col s12 light">THE WEDDING OF</h5>
                        <h1 class="header center text-lighten-2">Rio & Sheren</h1>
                        <h5 class="header col s12 light">Saturday, 25 June 2022</h5>
                    </div>
                    
                    <div class="row center">
                        <img class="ornament" src="img/ornament.png">
                    </div>
                    <br><br>

                </div>
            </div>
            <div class="parallax"><img src="img/background4.JPG" alt="Unsplashed background img 1"></div>
        </div>

        <!-- Nama Mempelai-->
        <div class="container">
            <div class="section" id="nama">
                <div class="row">
                    <div class="col s12">
                        <h3 class="center">The Wedding</h3>
                        <div class="center">
                            <img class="ornament" src="img/ornament.png">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6 offset-m3">
                        <div class="card">
                            <div class="card-image">
                                <img src="img/nama1.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6">
                        <h4 class="center">Nicholas Rio</h4>
                        <p class="light center">Putra dari Bapak Christophorus Handy Martono <br/> dan Ibu M.C. Cynthia Himawati</p>
                    </div>
                    <div class="col s12 m6">
                        <h4 class="center">Laurentia Hosiana Sheren</h4>
                        <p class="light center">Putri dari Bapak F.X.Soenlie (Loe Soen Lie) <br/> dan Ibu M.C. Susanti (Kwa Giok Sian)</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Event-->
        <div id="event">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12">
                            <h6 class="center">Kami akan saling menerimakan pemberkatan pernikahan</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6 offset-m3">
                            <div class="card event">
                                <div class="card-content center">
                                    <span class="card-title">PEMBERKATAN</span>
                                    <img class="ornament" src="img/ornament.png">
                                    <p>25 Juni 2022</p>
                                    <p>10.00 WIB</p>
                                    <i class="material-icons">location_on</i>
                                    <p><strong>Gereja Santo Martinus</strong></p>
                                    <p>Jalan Hercules 4, Kabupaten Bandung</p>
                                    <a class="location-link" href="https://goo.gl/maps/2r3qnxRTVKYfwWVP9"><p>Lihat Lokasi</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Live Streaming-->
        <div id="stream">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12">
                            <h3 class="center">Live Streaming</h3>
                            <div class="center">
                                <img class="ornament" src="img/ornament.png">
                            </div>
                        </div>
                    </div>
                    <div class="row center">
                        <h6>Anda dapat mengikuti pernikahan kami melalui link youtube di bawah ini</h6>
                        <a class="waves-effect waves-light btn red" href="http://tiny.cc/LivestreamRioSheren"><i class="material-icons left">videocam</i>Youtube</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Gallery-->
        <div id="gallery">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12">
                            <h3 class="center">Gallery</h3>
                            <div class="center">
                                <img class="ornament" src="img/ornament.png">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div class="carousel carousel-slider">
                                <a class="carousel-item"><img src="img/g1.JPG"></a>
                                <a class="carousel-item"><img src="img/g2.JPG"></a>
                                <a class="carousel-item"><img src="img/g3.JPG"></a>
                                <!-- <a class="carousel-item"><img src="img/g4.JPG"></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Wedding Gift-->
        <!--
        <div id="gift">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="col s12">
                            <h3 class="center">Wedding Gift</h3>
                            <div class="center">
                                <img class="ornament" src="img/ornament.png">
                            </div>
                        </div>
                    </div>
                    <div class="row center">
                        <h6>Seandainya Anda berkenan untuk memberikan tanda kasih, dapat melalui:</h6>
                    </div>
                    <div class="row center">
                        <div class="col s12 m4">
                            <div class="card">
                                <div class="card-image">
                                    <img src="img/QRIS.jpeg">
                                </div>
                            </div>
                        </div>
                        <div class="col s12 m8">
                            <h5><b>BCA a.n. Nicholas Rio</b></h5>
                            <h5>379 1423 617</h5>
                            <h6>atau</h6>
                            <h5><b>BCA a.n. Laurentia Hosiana Sheren</b></h5>
                            <h5>379 1467 916</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--JavaScript at end of body for optimized loading-->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="js/init.js"></script>
    </body>
</html>
